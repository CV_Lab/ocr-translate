<p align="center">
<a href="https://gitee.com/CV_Lab/ocr-translate">
<img src="https://pycver.gitee.io/ows-pics/imgs/ocr_translate_logo.png" alt="Simple Icons" >
</a>
<p align="center">
    基于Tesseract的OCR翻译系统
</p>
</p>
<p align="center">
<a href="./CodeCheck.md"><img src="https://img.shields.io/badge/CodeCheck-passing-success" alt="code check" /></a>
<a href="https://gitee.com/CV_Lab/ocr-translate/releases/v0.2"><img src="https://img.shields.io/badge/Releases-v0.2-green" alt="Releases Version" /></a>
<a href="https://huggingface.co/"><img src="https://img.shields.io/badge/%F0%9F%A4%97-Hugging%20Face-blue" alt="Hugging Face Spaces" /></a>
<a href="https://huggingface.co/spaces"><img src="https://img.shields.io/badge/🤗%20Hugging%20Face-Spaces-blue" alt="Hugging Face Spaces" /></a>
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det/blob/master/LICENSE"><img src="https://img.shields.io/badge/License-GPL--3.0-blue" alt="License" /></a>
</p>
<p align="center">
<a href="https://github.com/tesseract-ocr/tesseract"><img src="https://img.shields.io/badge/Tesseract%20OCR-5.1.0+-green" alt="Gradio Version" /></a>
<a href="https://huggingface.co/Helsinki-NLP"><img src="https://img.shields.io/badge/Helsinki--NLP-Opus--MT-blue" alt="Gradio Version" /></a>
<a href="https://github.com/gradio-app/gradio"><img src="https://img.shields.io/badge/Gradio-3.0.26+-orange" alt="Gradio Version" /></a>
<a href="#"><img src="https://img.shields.io/badge/Python-3.8%2B-blue?logo=python" alt="Python Version" /></a>
<a href="https://github.com/pre-commit/pre-commit"><img src="https://img.shields.io/badge/checks-pre--commit-brightgreen" alt="pre-commit"></a>
</p>

## 🚀 作者简介

曾逸夫，从事人工智能研究与开发；主研领域：计算机视觉；[YOLOv5官方开源项目代码贡献人](https://github.com/ultralytics/yolov5/graphs/contributors)；[YOLOv5 v6.1代码贡献人](https://github.com/ultralytics/yolov5/releases/tag/v6.1)；[Gradio官方开源项目代码贡献人](https://github.com/gradio-app/gradio/graphs/contributors)

❤️  Github：https://github.com/Zengyf-CVer

🔥 YOLOv5 官方开源项目PR ID：

- Save \*.npy features on detect.py `--visualize`：https://github.com/ultralytics/yolov5/pull/5701
- Fix `detect.py --view-img` for non-ASCII paths：https://github.com/ultralytics/yolov5/pull/7093
- Fix Flask REST API：https://github.com/ultralytics/yolov5/pull/7210
- Add yesqa to precommit checks：https://github.com/ultralytics/yolov5/pull/7511
- Add mdformat to precommit checks and update other version：https://github.com/ultralytics/yolov5/pull/7529
- Add TensorRT dependencies：https://github.com/ultralytics/yolov5/pull/8553

💡 YOLOv5 v6.1代码贡献链接：

- https://github.com/ultralytics/yolov5/releases/tag/v6.1

🔥 Gradio 官方开源项目PR ID：

- Create a color generator demo：https://github.com/gradio-app/gradio/pull/1872

<h2 align="center">🚀更新走势</h2>

- `2022-07-19` **⚡ [OCR Translate v0.2](https://gitee.com/CV_Lab/ocr-translate/releases/v0.2)正式上线**
- `2022-06-19` **⚡ [OCR Translate v0.1](https://gitee.com/CV_Lab/ocr-translate/releases/v0.1)正式上线**

<h2 align="center">🤗在线Demo</h2>

### ❤️ 快速体验

本项目提供了**在线demo**，点击下面的logo，进入**Hugging Face Spaces**中快速体验：

<div align="center" >
<a href="https://huggingface.co/spaces/Zengyf-CVer/ocr_translate">
<img src="https://pycver.gitee.io/ows-pics/imgs/huggingface_logo.png">
</a>
</div>

### 💡 Demo 列表

❤️ 点击列表中的链接，进入对应版本的**Hugging Face Spaces**界面中快速体验：

|                                     Demo 名称                                     | 输入类型 | 输出类型 |                                                                                                     状态                                                                                                      |
| :-----------------------------------------------------------------------------: | :--: | :--: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| 🚀 [OCR Translate v0.2](https://huggingface.co/spaces/Zengyf-CVer/ocr_translate) |  图片  |  文本  | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/ocr_translate/+.svg?label=demo%20status)](https://huggingface.co/spaces/Zengyf-CVer/ocr_translate) |

<h2 align="center">💎项目流程与用途</h2>

### 📌 项目整体流程

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/OCR_Translate_workflow.png">
</div>

### 📌 项目示例

#### 💡 OCR 文字提取（中文/英文）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/ocr_tr_v02_ocr.png">
</div>

#### 💡 翻译（中文-英文/英文-中文）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/ocr_tr_v02_translate.png">
</div>

<h2 align="center">💡项目结构</h2>

```
.
├── ocr-translate							# 项目名称
│   ├── opus-mt-en-zh						# Opus-MT翻译包
│   │   ├── config.json						# 配置文件
│   │   ├── flax_model.msgpack				# Flax模型
│   │   ├── metadata.bin					# PyTorch模型
│   │   ├── rust_model.ot					# Rust模型
│   │   ├── tf_model.h5						# TensorFlow模型
│   │   ├── tokenizer_config.json			# tokenizer配置
│   │   ├── ......							# 其他
│   ├── data								# 示例图片
│   ├── __init__.py							# 初始化文件
│   ├── ocr_translate.py					# 主运行文件
│   ├── LICENSE								# 项目许可
│   ├── CodeCheck.md						# 代码检查
│   ├── .gitignore							# git忽略文件
│   ├── README.md							# 项目说明
│   ├── setup.cfg							# pre-commit CI检查源配置文件
│   ├── .pre-commit-config.yaml				# pre-commit配置文件
│   └── requirements.txt					# 脚本依赖包
```

<h2 align="center">🔥安装教程</h2>

### ✅ 第一步：安装Tesseract OCR及其语言包（Ubuntu版）

#### 📌 安装Tesseract OCR

```shell
# 加入Tesseract OCR apt repo
sudo add-apt-repository ppa:alex-p/tesseract-ocr-devel
# 更新apt
sudo apt update
# 安装
sudo apt install tesseract-ocr
```

#### 📌 安装Tesseract OCR语言包

```shell
git clone https://github.com/tesseract-ocr/tessdata

# 加入环境变量
sudo vim ~/.bashrc
export TESSDATA_PREFIX=/home/zyf/tessdata

# 使环境变量生效
source ~/.bashrc

# 注：将script目录中的文件移动到tessdata根目录
```

### ✅ 第二步：创建conda环境

```shell
conda create -n ocr python==3.8
conda activate ocr # 进入环境
```

### ✅ 第三步：克隆

```shell
git clone https://gitee.com/CV_Lab/ocr-translate.git
```

### ✅ 第四步：安装OCR Translate依赖

```shell
cd ocr-translate
pip install -r ./requirements.txt -U
```

### ✅ 第五步：安装 Opus-MT 翻译包（离线版）

```shell
cd ocr-translate

# 中文-英文
git lfs clone https://huggingface.co/Helsinki-NLP/opus-mt-zh-en

# 英文-中文
git lfs clone https://huggingface.co/Helsinki-NLP/opus-mt-en-zh
```

<h2 align="center">⚡使用教程</h2>

```shell
python ocr_translate_v2.py # v0.2 推荐
python ocr_translate_v1.py # v0.1
```

### 💬 技术交流

- 如果你发现任何OCR Translate存在的问题或者是建议, 欢迎通过[Gitee Issues](https://gitee.com/CV_Lab/ocr-translate/issues)给我提issues。
- 欢迎加入CV Lab技术交流群

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/qq_group.jpg" width="20%">
</div>
